package addingRestrictions;
import java.util.Date;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommenUsedMethods.CommenMethods;

public class AddingRestrictionsTestCases { 
	private static WebDriver driver;
	static Logger lg = Logger.getLogger(addingRestrictions.AddingRestrictionsTestCases.class.getName());
	String baseUrl = "";
	CommenMethods CM = new CommenMethods(driver, baseUrl);
	static Date currentDate = new Date();
	static long time = currentDate.getTime();
	

	@BeforeTest
	public void setUp() throws Exception {
		CommenMethods CM = new CommenMethods(driver, baseUrl);
		CM.setDrivers();
		driver = CM.setUpDriversWebDrivers();
		System.out.println("CM.baseUrl" + CM.baseUrl);
		baseUrl = CM.baseUrl;
		driver.get(baseUrl);
		// wait for user name field to confirm page is loaded successfully 
		CM.wait("//input[@id='username']", "username field", driver);

	}
 
//Testing login
	@Test(priority = 0)
	public void TestingLogin() throws InterruptedException {
		// Clicking user name and sending my user name to login
		CM.click("//input[@id='username']", "user name field", driver);
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys("mohamedateif@gmail.com");
		//clicking continue button
		CM.click("//span[@class='css-t5emrf']/span[.='Continue']", "continue button", driver);
		// waiting password field
		CM.wait("//input[@class='Input__InputElement-sc-1o6bj35-0 bfCuIo']", "password field", driver);
		Thread.sleep(500);
		// Clicking password field and sending password to login
		CM.click("//input[@id='password']", "password field", driver);
		driver.findElement(By.xpath("//input[@id='password']")).clear();
		Thread.sleep(100);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("0122783434");
		// Clicking login button
		CM.click("//button[@id='login-submit']", "login button", driver);
		// waiting Test Page to confirm that login was Done successfully
		CM.wait("//div[@class='css-1p1ya2d']", "Test Page to confirm that login was Done successfully", driver);
 
	}

	// testing add view only restriction 
	@Test(priority = 1)
	public void ViewAndEditRestriction() throws Exception {
		// clicking the restriction icon
		CM.click("//button[@data-test-id='restrictions.dialog.button']", "restriction icon", driver);
		//waiting the restriction dialog
		CM.wait("//div[@data-test-id='restrictions-value-container']", "restriction dialog", driver);
		// clicking the restriction drop down menu
		CM.click("//div[@class=' css-5zed9i-control']//span[@class='sc-ifAKCX feLyHn']", "restriction dropdown menu", driver);
		Thread.sleep(1000);
		// clicking Viewing and editing restricted from drop down menu
		CM.click("//span[contains(text(),'Viewing and editing restricted')]", "Viewing and editing restricted", driver);
		
		//waiting the type user name field
		CM.wait("//div[@data-test-id='restrictions-dialog.users-and-groups-search']//div[@data-test-id='user-and-group-search']", "type username field", driver);
		Thread.sleep(1000);
		//clicking the type user name field
		CM.click("//div[@data-test-id='restrictions-dialog.users-and-groups-search']//div[@data-test-id='user-and-group-search']/div", "type username field", driver);
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[@style='display: inline-block;']/input")).sendKeys("medomedo");
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[@style='display: inline-block;']/input")).sendKeys(Keys.ENTER);
		Thread.sleep(200);
		//clicking type of permission drop down menu
		CM.click("(//span[contains(text(),'Can view and edit')])[1]", "type of permission drop down menu", driver);
		Thread.sleep(200);
		//clicking permission view and edit
		CM.click("(//span[contains(text(),'Can view and edit')])[3]", "permission view and edit", driver);
		Thread.sleep(200);
		//clicking add
		CM.click("//span[contains(text(),'Add')]", "add button", driver);
		//clicking apply
		CM.click("//span[contains(text(),'Apply')]", "apply button", driver);
		Thread.sleep(1000);
		//Logging out from this user to check if the access was actually given to the other user
		driver.get("https://mohamedateif11112.atlassian.net/wiki/logout.action");
		driver.close(); driver.quit(); 
		
		
		//verifying the restrictions is working fine
		
		//open new instant with the other user
		CM.setDrivers();
		driver = CM.setUpDriversWebDrivers();
		driver.get("https://mohamedateif11112.atlassian.net/wiki/spaces/TES/pages/229436/TestPage");
		// wait for user name field to confirm page is loaded successfully 
		CM.wait("//input[@id='username']", "username field", driver);

		//logging with the other user
		
		// Clicking user name and sending my user name to login
		CM.click("//input[@id='username']", "user name field", driver);
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys("MGalal@sure.com.sa");
		//clicking continue button
		CM.click("//span[@class='css-t5emrf']/span[.='Continue']", "continue button", driver);
		// waiting password field
		CM.wait("//input[@class='Input__InputElement-sc-1o6bj35-0 bfCuIo']", "password field", driver);
		Thread.sleep(1000);
		// Clicking password field and sending password to login
		CM.click("//input[@id='password']", "password field", driver);
		driver.findElement(By.xpath("//input[@id='password']")).clear();
		Thread.sleep(100);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("0122783434");
		// Clicking login button
		CM.click("//button[@id='login-submit']", "login button", driver);
		// waiting Test Page to confirm that login was Done successfully
		CM.wait("//div[@class='css-1p1ya2d']", "Test Page to confirm that login was Done successfully", driver);
		
		//clicking edit button
		CM.click("//button[@id='editPageLink']", "edit button", driver);
		//waiting publish button
		CM.wait("//button[@id='publish-button']", "waiting pub;ish button", driver);
		//verify the edit 
		CM.Verify("//button[@id='publish-button']", "Publish", "publish button", driver);
	}

	
	  @AfterTest 
	  public void tearDown() { 
		  driver.close(); driver.quit(); 
		  }
	 

}
